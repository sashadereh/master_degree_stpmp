//
//  DekanatTests.swift
//  DekanatTests
//
//  Created by Aleksandr Derekh on 4/1/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import XCTest
@testable import Dekanat

class DekanatTests: XCTestCase {
    
    //MARK: Student Class Tests
    
    // Confirm that the Student initializer returns a Stundet object when passed valid parameters.
    func testStudentInitializationSucceeds() {
        // Not empty name and valid rating
        let studentWithNotEmptyName = Student.init(name: "Student", rating: 0.0, photo: nil)
        XCTAssertNotNil(studentWithNotEmptyName)
    }
    
    func testStudentInitializationFails() {
        // Empty name and valid rating
        var invalidStudent = Student.init(name: "", rating: 0.0, photo: nil)
        XCTAssertNil(invalidStudent)
        
        // Empty name and invalid rating
        invalidStudent = Student.init(name: "Stident", rating: -1.0, photo: nil)
        XCTAssertNil(invalidStudent)
    }
}
