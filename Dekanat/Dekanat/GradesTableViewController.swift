//
//  GradesTableViewController.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/7/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

protocol GradesTableViewDelegate: class {
    
    func gradesChanged(grades: [Grade], rating: Double)
}

class GradesTableViewController: UITableViewController {
    var studentId: String?
    var grades = [Grade]()
    var rating = 0.0
    
    weak var delegate: GradesTableViewDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(GradesTableViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return grades.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return grades[section].grades.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < grades.count {
            return grades[section].subject.name
        }
        
        return nil
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        
        cell.textLabel!.text! = String(grades[indexPath.section].grades[indexPath.row])

        return cell
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "AddGrade":
            guard let addGradeViewController = segue.destination as? AddGradeViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            var subjects = [Subject]()
            for grade in grades {
                subjects.append(grade.subject)
            }
            
            addGradeViewController.subjects = subjects
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
 
    @IBAction func unwindToGradesList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? AddGradeViewController {
            
            let chosenSubject = sourceViewController.chosenSubject
            let gradeValue = sourceViewController.gradeValue
            
            // Add a new student's grade
            for (index, grade) in grades.enumerated() {
                if grade.subject.name == chosenSubject?.name {
                    grades[index].grades.append(gradeValue)
                    break
                }
            }
            
            APIConnection.updateGrades(id: studentId!, updatedGrades: self.grades)
            
            tableView.reloadData()
            
            // Count updated rating
            var updatedRating = 0.0
            var subjectsWithGrades = 0
            for grade in grades {
                if !grade.grades.isEmpty {
                    subjectsWithGrades += 1
                    updatedRating += grade.grades.average
                }
            }
            
            if subjectsWithGrades != 0 {
                updatedRating = updatedRating / Double(subjectsWithGrades)
            }
            
            rating = updatedRating
            
            APIConnection.setRating(id: studentId!, rating: updatedRating)
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        delegate?.gradesChanged(grades: grades, rating: rating)
        
        _ = navigationController?.popViewController(animated: true)
    }
}
