//
//  AddGradeViewController.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/7/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

class AddGradeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    var subjects = [Subject]()
    var chosenSubject: Subject?
    var gradeValue = 0
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var addGradeTextField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.isEnabled = false
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        addGradeTextField.delegate = self
        addGradeTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        
        chosenSubject = subjects[0]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subjects.count
    }
    
    // MARK: UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subjects[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        chosenSubject = subjects[row]
    }
    
    // MARK: Actions

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Private funcs
    @objc private func editingChanged(sender: UITextField) {
        if let insertedGrade = Int(self.addGradeTextField.text!) {
            if  (addGradeTextField.text?.isEmpty)! || (insertedGrade < 1 || insertedGrade > 10)
            {
                saveButton.isEnabled = false
                return
            } else {
                saveButton.isEnabled = true
                gradeValue = insertedGrade
            }
        } else {
            saveButton.isEnabled = false
            return
        }
    }
}
