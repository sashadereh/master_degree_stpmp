//
//  Address.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/6/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import Foundation
import MapKit

class Address: NSObject, MKAnnotation {
    var latitude = CLLocationDegrees(0.0)
    var longitude = CLLocationDegrees(0.0)
    var coordinate: CLLocationCoordinate2D
    
    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        self.latitude = latitude
        self.longitude = longitude
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(jsonFromBackendAPI: [String: Any]) {
        guard let latitude = jsonFromBackendAPI["latitude"] as? Double else {
            fatalError("Can not create Address instance from json without key \("latitude")")
        }
        
        guard let longitude = jsonFromBackendAPI["longitude"] as? Double else {
            fatalError("Can not create Address instance from json without key \("longitude")")
        }
        
        self.latitude = latitude
        self.longitude = longitude
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func isValid() -> Bool {
        return self.latitude != 0.0 && self.longitude != 0.0
    }
    
    func serialize_for_api() -> [String: Any] {
        var result = [String: Any]()
        
        result["latitude"] = self.latitude
        result["longitude"] = self.longitude
        
        return result
    }
}
