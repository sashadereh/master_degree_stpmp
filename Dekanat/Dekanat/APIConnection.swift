//
//  APIConnection.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/9/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import Foundation

class APIConnection {
    // MARK: Properties
    
    private static let apiUrl = "http://localhost:5000"
    private static let apiToken = "MIz44lUhQ8oUYuTIUTRFtkRCtj1T4uB3"
    
    static func addGroup(group: Group) {
        let parameters = ["api_token": self.apiToken, "payload": group.serializeForAPI()] as [String : Any]

        let url = URL(string: String(format: "%@/api/add_group/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func removeGroup(groupId: Int) {
        let parameters = ["api_token": self.apiToken, "payload": ["id": groupId]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/remove_group/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func addStudent(groupId: Int, student: Student) {
        let parameters = ["api_token": self.apiToken, "payload": ["group_id": groupId, "student": student.serialize_for_api()]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/add_student/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func removeStudent(groupId: Int, studentId: String) {
        let parameters = ["api_token": self.apiToken, "payload": ["group_id": groupId, "student_id": studentId]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/remove_student/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func editStudent(id: String, updatedStudent: Student) {
        let parameters = ["api_token": self.apiToken, "payload": ["id": id, "updated_student": updatedStudent.serialize_for_api()]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/edit_student/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func getGroups(callback: @escaping (NSArray?) -> Swift.Void) {
        let url = URL(string: String(format: "%@/api/get_groups/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            do {
                guard error == nil else {
                    let errorMessage = String(format: "Can not fetch groups due to error %@", (error?.localizedDescription)!)
                    print(errorMessage)
                    return
                }
                guard let data = data else {
                    let errorMessage = String(format: "Can not fetch groups due to error %@", "Data = nil")
                    print(errorMessage)
                    return
                }
                
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSArray
                callback(json)
            } catch let parseError {
                print(parseError)
            }
        })
        task.resume()
    }
    
    static func updateWorkingDays(groupId: Int, updatedWorkingDays: [WorkingDay]) {
        let workingDaysSerialized = NSMutableArray()
        for workingDay in updatedWorkingDays {
            workingDaysSerialized.add(workingDay.serialize_for_api())
        }
        
        let parameters = ["api_token": self.apiToken, "payload": ["id": groupId, "updated_working_days": workingDaysSerialized]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/update_working_days/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func addSubject(groupId: Int, subject: Subject) {
        let parameters = ["api_token": self.apiToken, "payload": ["group_id": groupId, "subject": subject.serialize_for_api()]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/add_subject/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func addGrade(id: String, grade: Grade) {
        let parameters = ["api_token": self.apiToken, "payload": ["id": id, "grade": grade.serialize_for_api()]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/add_grade/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func updateGrades(id: String, updatedGrades: [Grade]) {
        let gradesSerialized = NSMutableArray()
        for grade in updatedGrades {
            gradesSerialized.add(grade.serialize_for_api())
        }
        
        let parameters = ["api_token": self.apiToken, "payload": ["id": id, "updated_grades": gradesSerialized]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/update_grades/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
    
    static func setRating(id: String, rating: Double) {
        let parameters = ["api_token": self.apiToken, "payload": ["id": id, "rating": rating]] as [String : Any]
        
        let url = URL(string: String(format: "%@/api/set_rating/", self.apiUrl))!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
        })
        task.resume()
    }
}
