//
//  WorkingDay.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/5/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import Foundation

class WorkingDay {
    
    //MARK: Properties
    
    var dayNumber: Int
    var dayName: String
    var lessons = [Lesson]()
    
    //MARK: Initialization
    
    init(dayNumber: Int, dayName: String, lessons: [Lesson]?) {
        self.dayNumber = dayNumber
        self.dayName = dayName
        
        if let lessons = lessons {
            self.lessons = lessons
        }
    }
    
    init(json: [String: Any], dayNumber: Int) {
        self.dayNumber = dayNumber
        
        guard let dayName = json["weekDay"] as? String else {
            fatalError("Can not create WorkingDay instance from json without key \("weekDay")")
        }
        self.dayName = dayName
        
        var receivedLessons = [Lesson]()
        if let receivedLessonsAsJson = json["schedule"] as? NSArray {
            for receivedLessonAsJson in receivedLessonsAsJson {
                let receivedLesson = Lesson(json: receivedLessonAsJson as! [String : Any])
                receivedLessons.append(receivedLesson)
            }
        } else {
            fatalError("Can not create WorkingDay instance from json without key \("schedule")")
        }
        self.lessons = receivedLessons
    }
    
    init(jsonFromBackendAPI: [String: Any]) {
        guard let dayNumber = jsonFromBackendAPI["dayNumber"] as? Int else {
            fatalError("Can not create WorkingDay instance from json without key \("dayNumber")")
        }
        
        guard let dayName = jsonFromBackendAPI["dayName"] as? String else {
            fatalError("Can not create WorkingDay instance from json without key \("dayName")")
        }
        
        if let lessonsJson = jsonFromBackendAPI["lessons"] as? NSArray {
            for receivedLessonAsJson in lessonsJson {
                let receivedLesson = Lesson(jsonFromBackendAPI: receivedLessonAsJson as! [String : Any])
                self.lessons.append(receivedLesson)
            }
        }
        
        self.dayNumber = dayNumber
        self.dayName = dayName
    }
    
    func serialize_for_api() -> [String: Any] {
        var result = [String: Any]()
        
        result["dayNumber"] = self.dayNumber
        result["dayName"] = self.dayName
        
        let serializedLessons = NSMutableArray()
        for lesson in self.lessons {
            serializedLessons.add(lesson.serialize_for_api())
        }
        result["lessons"] = serializedLessons
        
        return result
    }
}
