//
//  StudentViewController.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/7/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

protocol StudentViewDelegate: class {
    func studentChanged(student: Student)
    
    func studentRemoved(id: String)
}

class StudentViewController: UIViewController, GradesTableViewDelegate, UITextViewDelegate {
    var student: Student?
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var emailLabel: UITextView!
    @IBOutlet weak var phoneLabel: UITextView!
    @IBOutlet weak var locationLabel: UITextView!
    @IBOutlet weak var gradesButton: UIButton!
    
    weak var delegate: StudentViewDelegate?
    
    // MARK: StudentTableViewDelegate
    
    func gradesChanged(grades: [Grade], rating: Double) {
        student?.grades = grades
        student?.rating = rating
        
        ratingLabel.text = String(format: "%.2f", rating)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gradesButton.isEnabled = false
        
        if let student = student {
            firstNameLabel.text = student.firstName
            lastNameLabel.text = student.lastName
            ratingLabel.text = String(format: "%.2f", student.rating)
            
            let urlForTel = String(format: "tel://%@", student.phone.onlyDigits())
            let attributedTelString = NSMutableAttributedString(string: student.phone)
            attributedTelString.addAttribute(.link, value: NSURL(string: urlForTel)!, range: NSMakeRange(0, student.phone.count))
            phoneLabel.attributedText = attributedTelString
            phoneLabel.delegate = self
            
            let urlForEmail = String(format: "mailto:%@", student.email)
            let attributedMailString = NSMutableAttributedString(string: student.email)
            attributedMailString.addAttribute(.link, value: NSURL(string: urlForEmail)!, range: NSMakeRange(0, student.email.count))
            emailLabel.attributedText = attributedMailString
            emailLabel.delegate = self
            
            let studentLocationCoordinatesAsString = String(format: "%f,%f", student.address.latitude, student.address.longitude)
            let urlForMapsApp = String(format: "http://maps.apple.com/maps?daddr=%@", studentLocationCoordinatesAsString)
            let attributedMapString = NSMutableAttributedString(string: studentLocationCoordinatesAsString)
            attributedMapString.addAttribute(.link, value: NSURL(string: urlForMapsApp)!, range: NSMakeRange(0, studentLocationCoordinatesAsString.count))
            locationLabel.attributedText = attributedMapString
            locationLabel.delegate = self
            
            photoImageView.image = student.photo
            
            if student.grades.count != 0 {
                gradesButton.isEnabled = true
            }
        }
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(StudentViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UITextViewDelegate
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return true
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "ShowGrades":
            guard let gradesTableViewController = segue.destination as? GradesTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            gradesTableViewController.studentId = (student?.id)!
            gradesTableViewController.grades = (student?.grades)!
            gradesTableViewController.delegate = self
        case "EditStudent":
            guard let editStudentViewController = segue.destination as? EditStudentViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            editStudentViewController.student = student!
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        delegate?.studentChanged(student: student!)
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: Actions
    
    @IBAction func onDeleteStudent(_ sender: UIButton) {
        delegate?.studentRemoved(id: (student?.id)!)
        
        _ = navigationController?.popViewController(animated: true)
    }
    
}
