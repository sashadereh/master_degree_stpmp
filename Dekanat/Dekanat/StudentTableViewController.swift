//
//  StudentTableViewController.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/2/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit
import os.log

protocol StudentTableViewDelegate: class {
    
    func studentsChanged(students: [Student])
    
    func groupRemoved(groupId: Int)
}

class StudentTableViewController: UITableViewController, StudentViewDelegate {
    //MARK: Properties
    
    var subjects = [Subject]()
    var students = [Student]()
    var groupID = 0
    
    func studentChanged(student: Student) {
        students[tableView.indexPathForSelectedRow!.row] = student
        tableView.reloadData()
    }
    
    func studentRemoved(id: String) {
        for (index, student) in students.enumerated().reversed() {
            if id == student.id {
                students.remove(at: index)
                tableView.reloadData()
                
                APIConnection.removeStudent(groupId: groupID, studentId: id)
                
                return
            }
        }
    }
    
    weak var delegate: StudentTableViewDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(StudentTableViewController.back(sender:)))
        navigationItem.leftBarButtonItem = newBackButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "StudentTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? StudentTableViewCell  else {
            fatalError("The dequeued cell is not an instance of StudentTableViewCell")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let student = students[indexPath.row]
        
        cell.nameLabel.text = String(format: "%@ %@", student.firstName, student.lastName)
        cell.photoImageView.image = student.photo
        cell.ratingLabel.text = String(format:"%.2f", student.rating)

        return cell
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "AddStudent":
            os_log("Adding a new student", log: OSLog.default, type: .debug)
        case "ShowStudentDetails":
            guard let studentViewDetailController = segue.destination as? StudentViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedStudentCell = sender as? StudentTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedStudentCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedStudent = students[indexPath.row]
            studentViewDetailController.student = selectedStudent
            studentViewDetailController.delegate = self
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        delegate?.studentsChanged(students: students)
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: Actions
    
    @IBAction func unwindToStudentsList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? EditStudentViewController, let student = sourceViewController.student {
            // Add a new student
            if (tableView.indexPathForSelectedRow == nil) {
                let newStudent = student
                for subject in subjects {
                    let newGrade = Grade(subject: subject, grades: [])
                    newStudent.grades.append(newGrade)
                }
                students.append(newStudent)
                
                APIConnection.addStudent(groupId: groupID, student: newStudent)
            }
            // Edit existing student
            else {
                students[tableView.indexPathForSelectedRow!.row] = student
                
                APIConnection.editStudent(id: student.id, updatedStudent: student)
            }
            tableView.reloadData()
        }
    }
    
    @IBAction func onDeleteGroup(_ sender: UIButton) {
        delegate?.groupRemoved(groupId: groupID)
        
        _ = navigationController?.popViewController(animated: true)
    }
}
