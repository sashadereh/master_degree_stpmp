//
//  GroupTableViewController.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/2/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

class GroupTableViewController: UITableViewController, StudentTableViewDelegate, ScheduleTableViewDelegate {
    var groups = [Group]()
    var selectedGroupIndex: IndexPath?
    var alertHelper = AlertHelper()
    var activityIndicatorView: ProgressHUDView!
    
    // MARK: StudentTableViewDelegate
    
    func studentsChanged(students: [Student]) {
        groups[selectedGroupIndex!.row].students = students
    }
    
    func groupRemoved(groupId: Int) {
        for (index, group) in groups.enumerated().reversed() {
            if groupId == group.id {
                groups.remove(at: index)
                tableView.reloadData()
                
                APIConnection.removeGroup(groupId: groupId)
                
                return
            }
        }
    }
    
    // MARK: ScheduleTableViewDelegate
    
    func workingDaysChanged(workingDays: [WorkingDay], scheduleUpdated: Bool) {
        if scheduleUpdated {
            groups[selectedGroupIndex!.row].workingDays = workingDays
            APIConnection.updateWorkingDays(groupId: groups[selectedGroupIndex!.row].id, updatedWorkingDays: workingDays)
            
            var updatedStudents = [Student]()
            for student in groups[selectedGroupIndex!.row].students {
                let updatedStudent = student
                for workingDay in workingDays {
                    for lesson in workingDay.lessons {
                        if updatedStudent.grades.first(where: { $0.subject.name == lesson.subject.name }) == nil {
                            let newGrade = Grade(subject: lesson.subject, grades: [])
                            updatedStudent.grades.append(newGrade)
                            APIConnection.addGrade(id: updatedStudent.id, grade: newGrade)
                        }
                    }
                }
                updatedStudents.append(updatedStudent)
            }
            groups[selectedGroupIndex!.row].students = updatedStudents
            
            for workingDay in workingDays {
                for lesson in workingDay.lessons {
                    if groups[selectedGroupIndex!.row].subjects.first(where: { $0.name == lesson.subject.name }) == nil {
                        groups[selectedGroupIndex!.row].subjects.append(lesson.subject)
                        APIConnection.addSubject(groupId: groups[selectedGroupIndex!.row].id, subject: lesson.subject)
                    }
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicatorView = ProgressHUDView(text: "Fetching groups")
        self.view.addSubview(self.activityIndicatorView)
        self.fetchGroups()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "GroupTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? GroupTableViewCell  else {
            fatalError("The dequeued cell is not an instance of GroupTableViewCell")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let group = groups[indexPath.row]
        
        cell.groupLabel.text = group.name
        
        return cell
    }

    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "AddGroup":
            break
        case "ShowGroupDetails":
            guard let groupViewDetailController = segue.destination as? StudentTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedGroupCellButton = sender as? UIButton else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            let point = selectedGroupCellButton.convert(CGPoint.zero, to: tableView as UIView)
            selectedGroupIndex = tableView.indexPathForRow(at: point)!
            groupViewDetailController.students = groups[selectedGroupIndex!.row].students
            groupViewDetailController.groupID = groups[selectedGroupIndex!.row].id
            groupViewDetailController.subjects = groups[selectedGroupIndex!.row].subjects
            groupViewDetailController.delegate = self
        case "ShowGroupSchedule":
            guard let scheduleViewController = segue.destination as? ScheduleTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedGroupCellButton = sender as? UIButton else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            let point = selectedGroupCellButton.convert(CGPoint.zero, to: tableView as UIView)
            selectedGroupIndex = tableView.indexPathForRow(at: point)!
            let selectedGroup = groups[selectedGroupIndex!.row]
            scheduleViewController.groupId = selectedGroup.id
            scheduleViewController.workingDays = selectedGroup.workingDays
            scheduleViewController.delegate = self
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    @IBAction func unwindToGroupsList(sender: UIStoryboardSegue) {
        if let segue = sender as? UIStoryboardSegueWithCompletion {
            segue.completion = {
                if let sourceViewController = sender.source as? AddGroupTableViewController {
                    // Add new group
                    if let chosenGroupIndex = sourceViewController.tableView.indexPathForSelectedRow {
                        let chosenGroup = sourceViewController.availableGroups[chosenGroupIndex.row]
                        if self.groups.first(where: { $0.name == chosenGroup.name }) != nil {
                            self.alertHelper.showAlert(fromController: self, title: "Forbidden", message: "You've already added this group")
                        } else {
                            self.groups.append(chosenGroup)
                            self.tableView.reloadData()
                            
                            APIConnection.addGroup(group: chosenGroup)
                        }
                    }
                }
            }
        }
    }
    
    private func fetchGroups() {
        self.activityIndicatorView.show()
        
        APIConnection.getGroups(callback: { groups in
            
            if let receivedGroups = groups {
                for group in receivedGroups {
                    let newGroup = Group(jsonFromBackendAPI: group as! [String : Any])
                    DispatchQueue.main.async {
                        self.groups.append(newGroup)
                        self.tableView.reloadData()
                    }
                }
            }
        })
        
        self.activityIndicatorView.hide()
    }
}
