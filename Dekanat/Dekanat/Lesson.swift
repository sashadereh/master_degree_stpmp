//
//  Lesson.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/5/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import Foundation

class Lesson {
    
    //MARK: Properties
    
    var subject: Subject
    var type: String
    var timeFrom: String
    var timeTo: String
    var roomName: String
    var subGroop: Int
    var weeks: [Int]
    
    //MARK: Initialization
    
    init(subject: Subject, type: String, timeFrom: String, timeTo: String, roomName: String, subGroop: Int, weeks: [Int]) {
        self.subject = subject
        self.type = type
        self.timeFrom = timeFrom
        self.timeTo = timeTo
        self.roomName = roomName
        self.subGroop = subGroop
        self.weeks = weeks
    }
    
    init(json: [String: Any]) {
        // Subject
        guard let subjectName = json["subject"] as? String else {
            fatalError("Can not create Lesson instance from json without key \("subject")")
        }
        
        // Lector
        guard let lectorContainer = json["employee"] as? NSArray else {
            fatalError("Can not create Lesson instance from json without key \("employee")")
        }
        var lectorName = ""
        if !(lectorContainer.count == 0) {
            if let lectorAsJson = lectorContainer[0] as? [String: Any] {
                guard let receivedLectorName = lectorAsJson["fio"] as? String else {
                    fatalError("Can not create Lesson instance from json without key \("fio") in lectorContainer")
                }
                lectorName = receivedLectorName
            }
        }
        
        // Lesson type
        guard let type = json["lessonType"] as? String else {
            fatalError("Can not create Lesson instance from json without key \("lessonType")")
        }
        
        // Time from
        guard let timeFrom = json["startLessonTime"] as? String else {
            fatalError("Can not create Lesson instance from json without key \("startLessonTime")")
        }
        
        // Time to
        guard let timeTo = json["endLessonTime"] as? String else {
            fatalError("Can not create Lesson instance from json without key \("endLessonTime")")
        }
        
        // Room name
        guard let roomContainer = json["auditory"] as? NSArray else {
            fatalError("Can not create Lesson instance from json without key \("auditory")")
        }
        var roomName = ""
        if !(roomContainer.count == 0) {
            roomName = (roomContainer[0] as? String)!
        }        
        
        // Subgroup
        guard let subGroup = json["numSubgroup"] as? Int else {
            fatalError("Can not create Lesson instance from json without key \("numSubgroup")")
        }
        
        // Weeks
        guard let weeks = json["weekNumber"] as? [Int] else {
            fatalError("Can not create Lesson instance from json without key \("weekNumber")")
        }
        
        self.subject = Subject(name: subjectName, lector: lectorName)
        self.type = type
        self.timeFrom = timeFrom
        self.timeTo = timeTo
        self.roomName = roomName
        self.subGroop = subGroup
        self.weeks = weeks
    }
    
    init(jsonFromBackendAPI: [String: Any]) {
        guard let subjectJson = jsonFromBackendAPI["subject"] as? [String: Any] else {
            fatalError("Can not create Lesson instance from json without key \("subject")")
        }
        
        guard let type = jsonFromBackendAPI["type"] as? String else {
            fatalError("Can not create Lesson instance from json without key \("type")")
        }
        
        guard let timeFrom = jsonFromBackendAPI["timeFrom"] as? String else {
            fatalError("Can not create Lesson instance from json without key \("timeFrom")")
        }
        
        guard let timeTo = jsonFromBackendAPI["timeTo"] as? String else {
            fatalError("Can not create Lesson instance from json without key \("timeTo")")
        }
        
        guard let roomName = jsonFromBackendAPI["roomName"] as? String else {
            fatalError("Can not create Lesson instance from json without key \("roomName")")
        }
        
        guard let subGroup = jsonFromBackendAPI["subGroop"] as? Int else {
            fatalError("Can not create Lesson instance from json without key \("subGroop")")
        }
        
        guard let weeks = jsonFromBackendAPI["weeks"] as? [Int] else {
            fatalError("Can not create Lesson instance from json without key \("weeks")")
        }
        
        self.subject = Subject(jsonFromBackendAPI: subjectJson)
        self.type = type
        self.timeFrom = timeFrom
        self.timeTo = timeTo
        self.roomName = roomName
        self.subGroop = subGroup
        self.weeks = weeks
    }
    
    func serialize_for_api() -> [String: Any] {
        var result = [String: Any]()
        
        result["subject"] = self.subject.serialize_for_api()
        result["type"] = self.type
        result["timeFrom"] = self.timeFrom
        result["timeTo"] = self.timeTo
        result["roomName"] = self.roomName
        result["subGroop"] = self.subGroop
        result["weeks"] = self.weeks
        
        return result
    }
}
