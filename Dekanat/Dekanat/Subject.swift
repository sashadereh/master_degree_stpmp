//
//  Subject.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/5/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import Foundation

class Subject {
    var id = UUID().uuidString
    var name: String
    var lector: String
    
    init(name: String, lector: String) {
        self.name = name
        self.lector = lector
    }
    
    init(jsonFromBackendAPI: [String: Any]) {
        guard let id = jsonFromBackendAPI["id"] as? String else {
            fatalError("Can not create Subject instance from json without key \("id")")
        }
        
        guard let name = jsonFromBackendAPI["name"] as? String else {
            fatalError("Can not create Subject instance from json without key \("name")")
        }
        
        guard let lector = jsonFromBackendAPI["lector"] as? String else {
            fatalError("Can not create Subject instance from json without key \("lector")")
        }
        
        self.id = id
        self.name = name
        self.lector = lector
    }
    
    func serialize_for_api() -> [String: Any] {
        var result = [String: Any]()
        
        result["id"] = self.id
        result["name"] = self.name
        result["lector"] = self.lector
        
        return result
    }
}
