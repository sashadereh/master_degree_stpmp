//
//  Grade.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/7/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import Foundation

class Grade {
    var subject: Subject
    var grades = [Int]()
    
    init(subject: Subject, grades: [Int]?) {
        self.subject = subject
        
        if let grades = grades {
            self.grades = grades
        }
    }
    
    init(jsonFromBackendAPI: [String: Any]) {
        guard let grades = jsonFromBackendAPI["grades"] as? [Int] else {
            fatalError("Can not create Grade instance from json without key \("grades")")
        }
        
        guard let subjectJson = jsonFromBackendAPI["subject"] as? [String: Any] else {
            fatalError("Can not create Grade instance from json without key \("subject")")
        }
        self.subject = Subject(jsonFromBackendAPI: subjectJson)
        
        self.grades = grades
    }
    
    func serialize_for_api() -> [String: Any] {
        var result = [String: Any]()
        
        result["subject"] = self.subject.serialize_for_api()
        result["grades"] = self.grades
        
        return result
    }
}
