//
//  StudentViewController.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/1/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit
import MapKit

class EditStudentViewController: UIViewController, UITextFieldDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    var student: Student?
    var studentAddress: Address?
    var alertHelper = AlertHelper()
    
    // MARK: Properties
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.isEnabled = false
        
        initMapCoordinates()
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        phoneNumberTextField.delegate = self
        
        firstNameTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        lastNameTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        phoneNumberTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        
        // Set up view if editing an existing student
        if let student = student {
            saveButton.isEnabled = true
            
            firstNameTextField.text = student.firstName
            lastNameTextField.text = student.lastName
            emailTextField.text = student.email
            phoneNumberTextField.text = student.phone
            photoImageView.image = student.photo
            studentAddress = student.address
            mapView.addAnnotation(student.address)
        }
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            return
        }
        
        let firstName = firstNameTextField.text ?? ""
        let lastName = lastNameTextField.text ?? ""
        let email = emailTextField.text ?? ""
        let phone = phoneNumberTextField.text ?? ""
        let photo = photoImageView.image
        
        guard let _ = studentAddress?.isValid() else {
            self.alertHelper.showAlert(fromController: self, title: "Invalid", message: "Student address is invalid")
            return
        }
        
        // Set the student to be passed to StudentTableViewController after the unwind segue
        let existingStudent = student
        // Create a new student
        student = Student(firstName: firstName.trimmingCharacters(in: .whitespacesAndNewlines), lastName: lastName.trimmingCharacters(in: .whitespacesAndNewlines),
                          email: email.trimmingCharacters(in: .whitespacesAndNewlines), phone: phone.trimmingCharacters(in: .whitespacesAndNewlines),
                          photo: photo!, address: self.studentAddress!)
        // We need to copy student grades and rating if it exists (edit)
        if existingStudent != nil {
            student!.grades = (existingStudent?.grades)!
            student!.rating = (existingStudent?.rating)!
            student!.id = (existingStudent?.id)!
        }
        
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddStudentMode = presentingViewController is UINavigationController
        
        if isPresentingInAddStudentMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The EditStudentViewController is not inside a navigation controller.")
        }
    }
    
    @IBAction func onMapLongPressed(_ sender: UILongPressGestureRecognizer) {
        if sender.state != UIGestureRecognizerState.began { return }
        let touchLocation = sender.location(in: mapView)
        let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
        
        // Remove previous tapped locations
        if let studentAddress = self.studentAddress {
            self.mapView.removeAnnotation(studentAddress)
        }
        
        self.studentAddress = Address(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
        
        // Add new tapped location
        mapView.addAnnotation(self.studentAddress!)
    }
    
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer) {
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .photoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // The info dictionary may contain multiple representations of the image. You want to use the original.
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        // Set photoImageView to display the selected image.
        photoImageView.image = selectedImage
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Private funcs
    @objc private func editingChanged(sender: UITextField) {
        if  (firstNameTextField.text?.isEmpty)! ||
            (lastNameTextField.text?.isEmpty)! ||
            (emailTextField.text?.isEmpty)! ||
            (phoneNumberTextField.text?.isEmpty)!
        {
            saveButton.isEnabled = false
            return
        } else {
            saveButton.isEnabled = true
        }
    }
    
    private func initMapCoordinates() {
        // Set Minsk as initial location
        let initialLocation = CLLocation(latitude: 53.902707, longitude: 27.561247)
        let regionRadius: CLLocationDistance = 5000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(initialLocation.coordinate,
                                                                  regionRadius, regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
}

