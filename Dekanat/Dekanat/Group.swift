//
//  Group.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/2/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

class Group {
    
    //MARK: Properties
    
    var name: String
    var id: Int
    var course: Int
    var students = [Student]()
    var workingDays = [WorkingDay]()
    var subjects = [Subject]()
    
    //MARK: Initialization
    
    init(name: String, id: Int, course: Int, students: [Student]?, workingDays: [WorkingDay]?) {
        self.name = name
        self.id = id
        self.course = course
        
        if let students = students {
            self.students = students
        }
        
        if let workingDays = workingDays {
            self.workingDays = workingDays
        }
    }
    
    // Init from Bsuir get all groups API call
    init(jsonFromBsuirAPI: [String: Any]) {
        guard let name = jsonFromBsuirAPI["name"] as? String else {
            fatalError("Can not create Group instance from json without key \("name")")
        }
        
        guard let id = jsonFromBsuirAPI["id"] as? Int else {
            fatalError("Can not create Group instance from json without key \("id")")
        }
        
        let course = jsonFromBsuirAPI["course"] as? Int ?? 0
        
        self.name = name
        self.id = id
        self.course = course
        self.students = [Student]()
    }
    
    // Init from backend API
    init(jsonFromBackendAPI: [String: Any]) {
        guard let name = jsonFromBackendAPI["name"] as? String else {
            fatalError("Can not create Group instance from json without key \("name")")
        }
        
        guard let id = jsonFromBackendAPI["id"] as? Int else {
            fatalError("Can not create Group instance from json without key \("id")")
        }
        
        guard let course = jsonFromBackendAPI["course"] as? Int else {
            fatalError("Can not create Group instance from json without key \("course")")
        }
        
        self.name = name
        self.id = id
        self.course = course
        
        if let studentsJson = jsonFromBackendAPI["students"] as? NSArray {
            for receivedStudentAsJson in studentsJson {
                let receivedStudent = Student(jsonFromBackendAPI: receivedStudentAsJson as! [String : Any])
                self.students.append(receivedStudent)
            }
        }
        
        if let subjectsJson = jsonFromBackendAPI["subjects"] as? NSArray {
            for receivedSubjectAsJson in subjectsJson {
                let receivedSubject = Subject(jsonFromBackendAPI: receivedSubjectAsJson as! [String : Any])
                self.subjects.append(receivedSubject)
            }
        }
        
        if let workingDaysJson = jsonFromBackendAPI["workingDays"] as? NSArray {
            for receivedWorkingDayAsJson in workingDaysJson {
                let receivedWorkingDay = WorkingDay(jsonFromBackendAPI: receivedWorkingDayAsJson as! [String : Any])
                self.workingDays.append(receivedWorkingDay)
            }
        }
    }
    
    func serializeForAPI() -> [String: Any] {
        var result = [String: Any]()
        
        result["id"] = self.id
        result["name"] = self.name
        result["course"] = self.course
        
        let serializedStudents = NSMutableArray()
        for student in self.students {
            serializedStudents.add(student.serialize_for_api())
        }
        result["students"] = serializedStudents
        
        let serializedSubjects = NSMutableArray()
        for subject in self.subjects {
            serializedSubjects.add(subject.serialize_for_api())
        }
        result["subjects"] = serializedSubjects
        
        let serializedWorkingDays = NSMutableArray()
        for workingDay in self.workingDays {
            serializedWorkingDays.add(workingDay.serialize_for_api())
        }
        result["workingDays"] = serializedWorkingDays
        
        return result
    }
}
