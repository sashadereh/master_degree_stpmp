//
//  AddGroupTableViewController.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/3/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

class AddGroupTableViewController: UITableViewController {
    var activityIndicatorView: ProgressHUDView!
    var availableGroups = [Group]()
    let alertHelper = AlertHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        self.activityIndicatorView = ProgressHUDView(text: "Fetching groups")
        self.view.addSubview(self.activityIndicatorView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.fetchGroups()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return availableGroups.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        
        cell.textLabel!.text! = availableGroups[indexPath.row].name
        
        return cell
    }

    // MARK: Navigation
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Private functions
    private func fetchGroups()
    {
        self.activityIndicatorView.show()
    
        let url = URL(string: "https://students.bsuir.by/api/v1/groups")!
        
        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        // Get available groups via BSUIR API
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            DispatchQueue.main.async {
                self.activityIndicatorView.hide()
            }
            
            guard error == nil else {
                let errorMessage = String(format: "Can not fetch groups due to error %@", (error?.localizedDescription)!)
                self.alertHelper.showAlert(fromController: self, title: "Error", message: errorMessage)
                return
            }
            guard let data = data else {
                let errorMessage = String(format: "Can not fetch groups due to error %@", "Data = nil")
                self.alertHelper.showAlert(fromController: self, title: "Error", message: errorMessage)
                return
            }
            
            do {
                // Parse received available groups
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSArray
                var availableGroupsTemp = [Group]()
                for receivedGroupAsJson in json! {
                    let newGroup = Group(jsonFromBsuirAPI: receivedGroupAsJson as! [String : Any])
                    availableGroupsTemp.append(newGroup)
                }
                
                // Sort available groups and add them to UITableView
                for group in availableGroupsTemp.sorted(by: { $0.name > $1.name }) {
                    DispatchQueue.main.async {
                        self.availableGroups.append(group)
                        self.tableView.reloadData()
                    }
                }
            } catch let parseError {
                print(parseError)
            }
        })
        task.resume()
    }
}
