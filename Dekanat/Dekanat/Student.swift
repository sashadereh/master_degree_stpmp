//
//  Student.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/2/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

class Student {
    
    //MARK: Properties
    
    // User enter this properties
    var id = UUID().uuidString
    var firstName: String
    var lastName: String
    var email: String
    var phone: String
    var address: Address
    var photo: UIImage
    
    // Generated based on user data
    var rating = 0.0
    var grades = [Grade]()

    init(firstName: String, lastName: String, email: String, phone: String, photo: UIImage, address: Address) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
        self.photo = photo
        self.address = address
    }
    
    // Init from backend API
    init(jsonFromBackendAPI: [String: Any]) {
        guard let id = jsonFromBackendAPI["id"] as? String else {
            fatalError("Can not create Student instance from json without key \("id")")
        }
        
        guard let firstName = jsonFromBackendAPI["firstName"] as? String else {
            fatalError("Can not create Student instance from json without key \("firstName")")
        }
        
        guard let lastName = jsonFromBackendAPI["lastName"] as? String else {
            fatalError("Can not create Student instance from json without key \("lastName")")
        }
        
        guard let email = jsonFromBackendAPI["email"] as? String else {
            fatalError("Can not create Student instance from json without key \("email")")
        }
        
        guard let phone = jsonFromBackendAPI["phone"] as? String else {
            fatalError("Can not create Student instance from json without key \("phone")")
        }
        
        guard let photoAsBase64EncodedString = jsonFromBackendAPI["photo"] as? String else {
            fatalError("Can not create Student instance from json without key \("photo")")
        }
        
        guard let addressJson = jsonFromBackendAPI["address"] as? [String: Any] else {
            fatalError("Can not create Student instance from json without key \("address")")
        }
        
        guard let rating = jsonFromBackendAPI["rating"] as? Double else {
            fatalError("Can not create Student instance from json without key \("rating")")
        }
        
        if let gradesJson = jsonFromBackendAPI["grades"] as? NSArray {
            for receivedGradeAsJson in gradesJson {
                let receivedGrade = Grade(jsonFromBackendAPI: receivedGradeAsJson as! [String : Any])
                self.grades.append(receivedGrade)
            }
        }
        
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
        self.rating = rating
        self.address = Address(jsonFromBackendAPI: addressJson )
        
        let photoDataDecoded : Data = Data(base64Encoded: photoAsBase64EncodedString, options: .ignoreUnknownCharacters)!
        self.photo = UIImage(data: photoDataDecoded)!
        }
    
    func serialize_for_api() -> [String: Any] {
        var result = [String: Any]()
        
        result["id"] = self.id
        result["firstName"] = self.firstName
        result["lastName"] = self.lastName
        result["email"] = self.email
        result["phone"] = self.phone
        result["address"] = self.address.serialize_for_api()
        
        let imageAsBytes = UIImageJPEGRepresentation(self.photo, 1.0)
        let base64EncodedPhotoData = imageAsBytes?.base64EncodedString()
        result["photo"] = base64EncodedPhotoData
        
        result["rating"] = self.rating
        
        let serializedGrades = NSMutableArray()
        for grade in self.grades {
            serializedGrades.add(grade.serialize_for_api())
        }
        result["grades"] = serializedGrades
        
        return result
    }
}
