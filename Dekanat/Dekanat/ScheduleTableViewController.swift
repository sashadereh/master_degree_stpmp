//
//  ScheduleTableViewController.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/4/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

protocol ScheduleTableViewDelegate: class {
    
    func workingDaysChanged(workingDays: [WorkingDay], scheduleUpdated: Bool)
}

class ScheduleTableViewController: UITableViewController {
    var workingDays = [WorkingDay]()
    var activityIndicatorView: ProgressHUDView!
    let alertHelper = AlertHelper()
    var groupId: Int?
    var scheduleUpdated = false
    
    weak var delegate: ScheduleTableViewDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let updateScheduleButton = UIBarButtonItem(title: "Update", style: UIBarButtonItemStyle.done,
                                                   target: self,
                                                   action: #selector(ScheduleTableViewController.updateSchedule(sender:)))
        self.navigationItem.rightBarButtonItem = updateScheduleButton
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ScheduleTableViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return workingDays.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workingDays[section].lessons.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < workingDays.count {
            return workingDays[section].dayName
        }
        
        return nil
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleTableViewCell", for: indexPath) as? ScheduleTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ScheduleTableViewCell")
        }

        cell.timeFromLabel.text = workingDays[indexPath.section].lessons[indexPath.row].timeFrom
        cell.timeToLabel.text = workingDays[indexPath.section].lessons[indexPath.row].timeTo
        cell.subjectNameLabel.text = workingDays[indexPath.section].lessons[indexPath.row].subject.name
        cell.typeNameLabel.text = workingDays[indexPath.section].lessons[indexPath.row].type
        cell.lectorNameLabel.text = workingDays[indexPath.section].lessons[indexPath.row].subject.lector
        cell.roomLabel.text = workingDays[indexPath.section].lessons[indexPath.row].roomName
        let subGroup = workingDays[indexPath.section].lessons[indexPath.row].subGroop
        if subGroup == 0 {
            cell.subGroupLabel.text = "A"
        } else {
            cell.subGroupLabel.text = String(subGroup)
        }
        let weeks = workingDays[indexPath.section].lessons[indexPath.row].weeks
        if weeks.count == 0 {
            cell.weeksLabel.text = "A"
        } else {
            cell.weeksLabel.text = String((weeks.compactMap { String($0) }).joined(separator: ","))
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    // MARK: Navigaion
    
    @objc func back(sender: UIBarButtonItem) {
        delegate?.workingDaysChanged(workingDays: self.workingDays, scheduleUpdated: self.scheduleUpdated)
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: Actions
    
    @objc func updateSchedule(sender: UIBarButtonItem) {
        self.activityIndicatorView = ProgressHUDView(text: "Fetching")
        self.view.addSubview(self.activityIndicatorView)
        self.activityIndicatorView.show()
        
        let urlAsString = String(format: "https://students.bsuir.by/api/v1/studentGroup/schedule?id=%d", groupId!)
        
        print("Going to fetch schedule from url = \(urlAsString)")
        
        let url = URL(string: urlAsString)!
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        // Get group schedule via BSUIR API
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            DispatchQueue.main.async {
                self.activityIndicatorView.hide()
            }
            
            guard error == nil else {
                let errorMessage = String(format: "Can not fetch schedule for the group id %d due to error %@", self.groupId!, (error?.localizedDescription)!)
                self.alertHelper.showAlert(fromController: self, title: "Error", message: errorMessage)
                return
            }
            guard let data = data else {
                let errorMessage = String(format: "Can not fetch schedule for the group due to error %@", "Data = nil")
                self.alertHelper.showAlert(fromController: self, title: "Error", message: errorMessage)
                return
            }
            
            do {
                self.scheduleUpdated = true
                // Parse received schedule
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                if let receivedWorkingDaysAsJson = json!["examSchedules"] as? NSArray {
                    for (index, receivedWorkingDayAsJson) in receivedWorkingDaysAsJson.enumerated() {
                        let newWorkingDay = WorkingDay(json: receivedWorkingDayAsJson as! [String : Any], dayNumber: index)
                        DispatchQueue.main.async {
                            self.workingDays.append(newWorkingDay)
                            self.tableView.reloadData()
                        }
                    }
                }
                if let receivedWorkingDaysAsJson = json!["schedules"] as? NSArray {
                    for (index, receivedWorkingDayAsJson) in receivedWorkingDaysAsJson.enumerated() {
                        let newWorkingDay = WorkingDay(json: receivedWorkingDayAsJson as! [String : Any], dayNumber: index)
                        DispatchQueue.main.async {
                            self.workingDays.append(newWorkingDay)
                            self.tableView.reloadData()
                        }
                    }
                }
            } catch let parseError {
                print(parseError)
            }
        })
        task.resume()
    }
}
