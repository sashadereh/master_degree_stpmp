//
//  GroupTableViewCell.swift
//  Dekanat
//
//  Created by Aleksandr Derekh on 4/2/18.
//  Copyright © 2018 DerekhApps. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

    // MARK: Outlets
    
    @IBOutlet weak var groupLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: Actions
    
}
