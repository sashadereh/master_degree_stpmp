from api import flask_core
from api.db.mongo import MongoDB, GROUP_DOCUMENT_NAME, DEKANAT_DB

from flask import render_template, request, redirect, url_for, session, abort, jsonify

import traceback

DB_CONNECTION = MongoDB.instance()


API_TOKEN = "MIz44lUhQ8oUYuTIUTRFtkRCtj1T4uB3"


@flask_core.route('/')
@flask_core.route('/index')
@flask_core.route("/api/health_check/", methods=['GET'])
def health_check():
    return "API service operational"


def validate_api_call(request):
    request_data = request.get_json(force=True)
    if "api_token" not in request_data:
        abort(403)

    if request_data["api_token"] != API_TOKEN:
        abort(403)

    return request_data["payload"]


def get_response(code, message):
    response = jsonify(message=message)
    response.status_code = code
    return response


# Groups

@flask_core.route("/api/get_groups/", methods=['GET'])
def get_groups():
    response = []
    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]
        cursor = db.find({})
        for group_data in cursor:
            group_data_copy = group_data
            group_data_copy["_id"] = str(group_data["_id"])
            response.append(group_data_copy)
        return jsonify(response)
    except Exception as error:
        print("Failed to get groups due to {}".format(error))
        return get_response(500, "Failed")


@flask_core.route("/api/add_group/", methods=['POST'])
def add_group():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]
        cursor = db.find_one({"id": request_data["id"]})
        if not cursor:
            db.insert(request_data)
        else:
            raise Exception("can not insert group that is already added")

    except Exception as error:
        print("Failed to add group due to {}".format(error))
        return get_response(500, "Failed")

    return get_response(200, "Group added successfully")


@flask_core.route("/api/update_working_days/", methods=['POST'])
def update_working_days():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]

        db.update({"id": request_data["id"]}, {"$set": {"workingDays": request_data["updated_working_days"]}})

    except Exception as error:
        print("Failed to add working days due to {}".format(error))
        print(traceback.print_exc())
        return get_response(500, "Failed")

    return get_response(200, "Working days updated successfully")


@flask_core.route("/api/add_subject/", methods=['POST'])
def update_subjects():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]

        db.update({"id": request_data["group_id"]}, {"$addToSet": {"subjects": request_data["subject"]}})

    except Exception as error:
        print("Failed to update subjects due to {}".format(error))
        print(traceback.print_exc())
        return get_response(500, "Failed")

    return get_response(200, "Subjects updated successfully")


@flask_core.route("/api/remove_group/", methods=['POST'])
def remove_group():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]
        cursor = db.find_one({"id": request_data["id"]})
        if not cursor:
            raise Exception("can not remove group that is not exists")
        else:
            db.remove({"id": request_data["id"]})

    except Exception as error:
        print("Failed to add remove due to: {}".format(error))
        return get_response(500, "Failed")

    return get_response(200, "Group removed successfully")


# Users

@flask_core.route("/api/add_student/", methods=['POST'])
def add_student():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]
        db.update({"id": request_data["group_id"]}, {"$addToSet": {"students": request_data["student"]}})
    except Exception as error:
        print("Failed to add student due to {}".format(error))
        print(traceback.print_exc())
        return get_response(500, "Failed")

    return get_response(200, "Student added successfully")


@flask_core.route("/api/edit_student/", methods=['POST'])
def edit_student():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]

        cursor = db.find_one({"students.id": request_data["id"]})
        if not cursor:
            raise Exception("can not edit user with id {} that is not exists".format(request_data["id"]))
        else:
            db.update({"students.id": request_data["id"]}, {"$set": {"students.$": request_data["updated_student"]}})

    except Exception as error:
        print("Failed to edit user due to {}".format(error))
        print(traceback.print_exc())
        return get_response(500, "Failed")

    return get_response(200, "User edited successfully")


@flask_core.route("/api/remove_student/", methods=['POST'])
def remove_student():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]
        db.update({"id": request_data["group_id"]}, {"$pull": {"students": {"id": request_data["student_id"]}}})
    except Exception as error:
        print("Failed to remove student due to {}".format(error))
        print(traceback.print_exc())
        return get_response(500, "Failed")

    return get_response(200, "Student removed successfully")


@flask_core.route("/api/add_grade/", methods=['POST'])
def add_grades():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]
        db.update({"students.id": request_data["id"]}, {"$addToSet": {"students.$.grades": request_data["grade"]}})
    except Exception as error:
        print("Failed to add grade due to {}".format(error))
        print(traceback.print_exc())
        return get_response(500, "Failed")

    return get_response(200, "Grade added successfully")


@flask_core.route("/api/update_grades/", methods=['POST'])
def update_grades():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]
        db.update({"students.id": request_data["id"]}, {"$set": {"students.$.grades": request_data["updated_grades"]}})
    except Exception as error:
        print("Failed to update grades due to {}".format(error))
        print(traceback.print_exc())
        return get_response(500, "Failed")

    return get_response(200, "Grades updated successfully")


@flask_core.route("/api/set_rating/", methods=['POST'])
def set_rating():
    request_data = validate_api_call(request)

    try:
        db = DB_CONNECTION.client[DEKANAT_DB][GROUP_DOCUMENT_NAME]
        db.update({"students.id": request_data["id"]}, {"$set": {"students.$.rating": request_data["rating"]}})
    except Exception as error:
        print("Failed to set rating due to {}".format(error))
        print(traceback.print_exc())
        return get_response(500, "Failed")

    return get_response(200, "Rating set successfully")