from pymongo import MongoClient

MONGO_DB_HOST = "localhost"

DEKANAT_DB = "DekanatDB"
MONGO_DB_PORT = 27017

GROUP_DOCUMENT_NAME = "Group"


class MongoDB(object):
    _instance = None

    def __init__(self, host, port):
        self.client = MongoClient(host=host, port=port)

    def __del__(self):
        ((hasattr(self, 'client') and self.client) and self.client.close())

    @staticmethod
    def instance():
        """
        return mongoDB instance if connection was open. Create new if not
        :rtype: MongoDB
        """
        if not MongoDB._instance:
            MongoDB._instance = MongoDB(MONGO_DB_HOST, MONGO_DB_PORT)

        return MongoDB._instance
