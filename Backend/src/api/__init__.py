from flask import Flask

flask_core = Flask(__name__)

from api.routes import api_routes
